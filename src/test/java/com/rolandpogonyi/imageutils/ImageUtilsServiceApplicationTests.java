package com.rolandpogonyi.imageutils;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import java.io.File;
import java.io.FileInputStream;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMultipartHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import com.rolandpogonyi.imageutils.controller.ResizeController;
import com.rolandpogonyi.imageutils.model.ResizeRequest;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ImageUtilsServiceApplicationTests {

	@Autowired
	private ResizeController resizeController;
	@Autowired
	private MockMvc mockMvc;
	
	@Test
	public void contextLoads() {
		assertThat(resizeController).isNotNull();
	}
	
	@Test
	public void testUpload() throws Exception {
		
		File file = new ClassPathResource("Koala.jpg").getFile();
		
		MockMultipartFile mpf = new MockMultipartFile("file", file.getName(),
				"image/jpeg", new FileInputStream(file));
		
		MockMultipartHttpServletRequestBuilder builder = 
				MockMvcRequestBuilders.fileUpload("/v1/resize");
		
		builder.with(r -> {
			r.setMethod("POST");
			r.setParameter("type", ResizeRequest.LONGDIM);
			r.setParameter("dim", "300");
			return r;
		});
		
		builder.file(mpf);
		
		MvcResult result = mockMvc.perform(builder)
				.andExpect(status().isOk())
				.andReturn();
		
		assertThat(result.getResponse().getStatus())
			.isEqualTo(HttpStatus.OK.value());
	}

}
