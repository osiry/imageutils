/**
 * @author Roland
 */

$(document).on('change', '#resizeType', function() {
	UI.toggleModes($(this));
}).on('change', 'input[type=file]', function() {
	$(this).data('selected', 'true');
});

var UI = {
	toggleModes: function(elem) {
		if (elem.val() == 'longdim') {
			$('#longDimCont').show();
			$('#exactCont').hide();
		}
		else {
			$('#exactCont').show();
			$('#longDimCont').hide();
		}
	},
	validate() {
		var isValid = $('input[type=file]').data('selected') == 'true';
		
		if (!isValid) {
			alert('Select an image to resize.');
		}
		
		return isValid;
	}
}