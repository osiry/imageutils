package com.rolandpogonyi.imageutils;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * The main class of the application
 * @author Roland
 *
 */
@SpringBootApplication
public class ImageUtilsServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ImageUtilsServiceApplication.class, args);
	}
}
