package com.rolandpogonyi.imageutils.model;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.List;
import org.apache.commons.lang3.reflect.FieldUtils;

/**
 * 
 * @author Roland
 *
 */
public abstract class Request {
	
	/**
	 * Handy method for easy logging, uses reflection to get field values
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("\n");
		List<Field> fields = FieldUtils.getAllFieldsList(this.getClass());
		
		fields.forEach(f -> {
			f.setAccessible(true);
			if (!Modifier.isFinal(f.getModifiers())) {
				try {
					sb.append(f.getName() + " : " + f.get(this) + "\n");
				} catch (IllegalArgumentException | IllegalAccessException e) {
					e.printStackTrace();
				}
			}
		});
		
		return sb.toString();
	}
}
