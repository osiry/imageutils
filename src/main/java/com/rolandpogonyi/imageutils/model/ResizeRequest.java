package com.rolandpogonyi.imageutils.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Convenience class to map request parameters
 * @author Roland
 *
 */
public class ResizeRequest extends Request {
	
	public static final String LONGDIM = "longdim";
	public static final String EXACT = "exact";
	
	@JsonProperty("dim")
	private Integer longDim;
	@JsonProperty("width")
	private Integer width;
	@JsonProperty("height")
	private Integer height;
	@JsonProperty("type")
	private String type;

	public Integer getLongDim() {
		return longDim;
	}

	public void setLongDim(Integer longDim) {
		this.longDim = longDim;
	}

	public Integer getWidth() {
		return width;
	}

	public void setWidth(Integer width) {
		this.width = width;
	}

	public Integer getHeight() {
		return height;
	}

	public void setHeight(Integer height) {
		this.height = height;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}
