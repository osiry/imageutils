package com.rolandpogonyi.imageutils.requestutils;

import org.springframework.util.MultiValueMap;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rolandpogonyi.imageutils.model.ResizeRequest;

/**
 * Resize request implementation
 * @author Roland
 *
 */
public class ResizeRequestParser extends RequestParser {
	
	/**
	 * @return ResizeRequest
	 */
	@Override
	public ResizeRequest get(MultiValueMap<String, Object> params) {
		
		ObjectMapper mapper = new ObjectMapper();
		ResizeRequest request = mapper.convertValue(convertToHashMap(params),
				ResizeRequest.class);
		
		return request;
	}
}
