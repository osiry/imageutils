package com.rolandpogonyi.imageutils.requestutils;

import java.util.Collection;
import java.util.HashMap;

import org.springframework.util.MultiValueMap;
import com.rolandpogonyi.imageutils.model.Request;

/**
 * Request utilities
 * @author Roland
 *
 */
abstract class RequestParser {
	
	/**
	 * 
	 * @param params Map of request parameters
	 * @return an instance of a Request implementation
	 */
	public abstract Request get(MultiValueMap<String, Object> params);
	
	/**
	 * Utility method to convert various collections to HashMap
	 * @param collection a collection to be converted to HashMap
	 * @return a HashMap if the conversion is successful
	 */
	@SuppressWarnings("unchecked")
	protected static HashMap<String, Object> convertToHashMap(Object collection) {
		HashMap<String, Object> hashMap = new HashMap<>();
		if (collection instanceof MultiValueMap) {
			((MultiValueMap<String, Object>) collection).forEach((k, v) -> {
				if (v instanceof Collection) {
					if (v.size() > 1) {
						hashMap.put(k, v);
					} else {
						hashMap.put(k, v.get(0));
					}
				} else {
					hashMap.put(k, v);
				}

			});
			return hashMap;
		} else {
			throw new RuntimeException("HashMap conversion not yet implemented");
		}
	}
}
