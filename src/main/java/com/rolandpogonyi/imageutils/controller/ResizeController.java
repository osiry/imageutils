package com.rolandpogonyi.imageutils.controller;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import javax.imageio.ImageIO;
import org.apache.tika.Tika;
import org.imgscalr.Scalr;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import com.rolandpogonyi.imageutils.model.ResizeRequest;
import com.rolandpogonyi.imageutils.requestutils.ResizeRequestParser;

/**
 * Controller responsibe for resizing images.
 * @author Roland
 *
 */
@RestController
public class ResizeController {
	
	private Logger log = LoggerFactory.getLogger(ResizeController.class);
	
	/**
	 * Resizes an image. Falls back on original dimensions if size not supplied.
	 * @param file the file to be resized
	 * @param params request parameters
	 * @return resized image
	 * @throws IOException if an error happens while reading or writing the file
	 */
	@RequestMapping(value="/v1/resize", method=RequestMethod.POST,
			produces=MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<InputStreamResource> resize (
			@RequestParam("file") MultipartFile file,
			@RequestParam MultiValueMap<String, Object> params) throws IOException {
		
		ResizeRequest request = new ResizeRequestParser().get(params);
		
		log.info(request.toString());
		
		String type = new Tika().detect(file.getInputStream());
		
		// add more types as implemented
		Set<String> acceptedTypes = new HashSet<>(Arrays.asList(new String[] {
				"image/jpg",
				"image/jpeg"
		}));
		
		if (!acceptedTypes.contains(type)) {
			return new ResponseEntity<InputStreamResource>(
					new InputStreamResource(
							new ByteArrayInputStream(
									"Oops, that doesn't look like a JPG.".getBytes())),
					HttpStatus.UNSUPPORTED_MEDIA_TYPE);
		}
		
		BufferedImage img = ImageIO.read(file.getInputStream());
		
		if (img == null) {
			throw new RuntimeException("Could not read image!");
		}
		
		BufferedImage resized = null;
		
		if (request.getType().equals(ResizeRequest.LONGDIM)) {
			resized = Scalr.resize(img, Optional.ofNullable(
					request.getLongDim()).orElse(
							Math.max(img.getWidth(), img.getHeight())));
		}
		else {
			resized = Scalr.resize(img, Scalr.Method.BALANCED,
					Scalr.Mode.FIT_EXACT,
					Optional.ofNullable(request.getWidth())
						.orElse(img.getWidth()),
					Optional.ofNullable(request.getHeight())
						.orElse(img.getHeight()));
		}
		
		if (resized == null) {
			throw new RuntimeException("Could not resize image!");
		}
		
		String filename = "tmp/resized_" + file.getOriginalFilename();
		File output = new File(filename);
		output.mkdirs();
		ImageIO.write(resized, "jpg", output);
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.IMAGE_JPEG);
		headers.setContentLength(output.length());
		headers.setContentDispositionFormData("attachment", filename);
		
		InputStreamResource is = new InputStreamResource(
				new FileInputStream(output));
		
		return new ResponseEntity<InputStreamResource>(is, headers, HttpStatus.OK);
	}
}
